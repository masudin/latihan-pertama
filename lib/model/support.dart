import 'package:json_annotation/json_annotation.dart';

part 'support.g.dart';

@JsonSerializable()
class Support {
  final String url;
  final String text;

  Support(
      {required this.url,
      required this.text});

  //map -> model
  factory Support.fromJson(Map<String, dynamic> data) => _$SupportFromJson(data);

  //model->map
  Map<String, dynamic> toJson() => _$SupportToJson(this);
}