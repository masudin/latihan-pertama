import 'dart:convert';
import 'model/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  Future<UserModel?> getDataUser() async {
    Uri uri = Uri.parse("https://reqres.in/api/users/2");
    var response = await http.get(uri);

    if (response.statusCode != 200) {
      print("user tidak ada");
      return null;
    } else {
      Map<String, dynamic> data =
          (json.decode(response.body) as Map<String, dynamic>);
      return UserModel.fromJson(data);
      print(data);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("dummy reqres.in"),
      ),
      body: FutureBuilder<UserModel?>(
          future: getDataUser(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else {
              if (snapshot.hasData) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 50,
                        backgroundImage: NetworkImage(snapshot.data!.data.avatar),
                      ),
                      Text("id : ${snapshot.data!.data.id}"),
                      Text("email : ${snapshot.data!.data.email}"),
                      Text("nama : ${snapshot.data!.data.first_name} ${snapshot.data!.data.last_name}"),

                      SizedBox(height: 20,),

                      Text("url : ${snapshot.data!.support.url}" ),
                      Text("text : ${snapshot.data!.support.text}")
                    ],
                  ),
                );
              } else {
                return Center(
                  child: Text("Tidak ada data"),
                );
              }
            }
          }),
    );
  }
}
