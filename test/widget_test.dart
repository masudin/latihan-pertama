
import 'package:http/http.dart' as http;

void main() async {
  await getDataUser();
}


Future getDataUser() async {
    Uri uri = Uri.parse("https://reqres.in/api/users/1");
    var response = await http.get(uri);

    if(response.statusCode != 200){
      print("user tidak ada");
    }else{
      print(response.body);
    }
}